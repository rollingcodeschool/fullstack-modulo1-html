/** video */
var video = document.getElementById("video");
var btnVideo = document.getElementById("btnPlay");
var btnPause = document.getElementById("btnPause");
var btnMute = document.getElementById("btnMuted");

btnVideo.addEventListener("click", (e) => {
    video.play();
})

btnPause.addEventListener("click", () => {
    video.pause();
})

btnMute.addEventListener("click", () => {
    video.muted = !video.muted;
});


/** diaglo */
var dialog = document.getElementById("dialog");
var btnOpenDialog = document.getElementById("btnOpenDialog");
var btnCloseDialog = document.getElementById("btnCloseDialog");

btnOpenDialog.addEventListener("click", () => {
    dialog.setAttribute("open", true);
});

btnCloseDialog.addEventListener("click", () => {
    dialog.removeAttribute("open");
})